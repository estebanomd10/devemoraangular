import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import CatalogOption from '../interfaces/catalogOption.inteface';


@Injectable()
export class GlobalFunctionsService {

  constructor(private router: Router) { }

  /**
   * Genera un JSON de opciones a partir de la respuesta que entrega el servicio de Catálogos.
   * 
   * @param source: Respuesta que entrega el servicio de Catálogos.
   * @param additionalOptions: Opciones adicionales (opcional).
   */
  generateOptionsJSON(source: CatalogOption[], additionalOptions?: any) : CatalogOption[] {

    const options: CatalogOption[] = [];

    if(additionalOptions) {

      for (const option of additionalOptions) {
        options.push({id: option['id'], label: option['label'], enabled: option['enabled'], selected: option['selected']});
      }
    }

    for (const option of source) {
      // 'idDato' y 'nombreDato' son los elementos que nos interesan de la respuesta del servicio.
      options.push({id: option['idDato'], label: option['nombreDato'], enabled: true});
    }

    console.info("OPTIONSOPTIONSOPTIONSOPTIONSOPTIONSOPTIONSOPTIONSOPTIONSOPTIONSOPTIONSOPTIONSOPTIONSOPTIONS")
    console.info(options);
    console.info("OPTIONSOPTIONSOPTIONSOPTIONSOPTIONSOPTIONSOPTIONSOPTIONSOPTIONSOPTIONSOPTIONSOPTIONSOPTIONS")

    return options;
  }

  /**
   * Genera un JSON de opciones a partir de un arreglo o lista.
   * 
   * @param source: Arreglo o lista.
   * @param additionalOptions: Opciones adicionales (opcional).
   */
  generateOptionsJSONfromList(source: any, additionalOptions?: any) : CatalogOption[]  {

    let options: CatalogOption[] = [];

    if(additionalOptions) {

      for (const option of additionalOptions) {
        options.push({id: option['id'], label: option['label'], enabled: option['enabled'], selected: option['selected']});
      }
    }

    for (const option of source) {
      // Como los elementos del arreglo no tienen id, asignamos el mismo elemento (option) como id y como label.
      options.push({id: option, label: option, enabled: true});
    }

    console.info("OPTIONSOPTIONSOPTIONSOPTIONSOPTIONSOPTIONSOPTIONSOPTIONSOPTIONSOPTIONSOPTIONSOPTIONSOPTIONS")
    console.info(options);
    console.info("OPTIONSOPTIONSOPTIONSOPTIONSOPTIONSOPTIONSOPTIONSOPTIONSOPTIONSOPTIONSOPTIONSOPTIONSOPTIONS")

    return options;
  }

  gotoHome() {
    this.router.navigate(['/home']);
  }

}
