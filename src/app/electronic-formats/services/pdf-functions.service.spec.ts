import { TestBed, inject } from '@angular/core/testing';

import { PdfFunctionsService } from './pdf-functions.service';

describe('PdfFunctionsService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [PdfFunctionsService]
    });
  });

  it('should be created', inject([PdfFunctionsService], (service: PdfFunctionsService) => {
    expect(service).toBeTruthy();
  }));
});
