import { Injectable } from '@angular/core';

@Injectable()
export class PdfFunctionsService {

  constructor() { }

  generateJSONForPDFService(formatJSON) {

    let json = {
      producto: {  
          certificate: {  
            day: "",
            month: "",
            year: "",
            officeName: this.getFinalPropertyValue(formatJSON, 'officeName'),
            codeClient: "",
            name: "",
            rfc: "",
            pf: "",
            pfe: "",
            dateBirth: "",
            nationality: "",
            placeIncorp: "",
            citizenship: "",
            address: "",
            colony: "",
            delegation: "",
            state: "",
            countryRecidency: "",
            countryBirth: "",
            federEntity: "",
            accountResponsExtYes: "",
            accountResponsExtNo: "",
            taxofficials:[  
                // {  
                //   countryTax: "BOLIVIA",
                //   rfcTin:321654877798
                // },
                // {  
                //   countryTax: "BOLIVIA",
                //   rfcTin:321654877798
                // },
                // {  
                //   countryTax: "BOLIVIA",
                //   rfcTin:321654877798
                // },
                // {  
                //   countryTax: "BOLIVIA",
                //   rfcTin:321654877798
                // }
            ]
          },
          anexed:{  
            contract: "",
            cotitulars:[  
                //?
            ],
            reca: ""
          },
          cliente:{  
            dia: this.getFinalPropertyValue(formatJSON, 'dia'),
            mes: this.getFinalPropertyValue(formatJSON, 'mes'),
            anyo: this.getFinalPropertyValue(formatJSON, 'anyo'),
            hora: this.getFinalPropertyValue(formatJSON, 'hora'),
            direccion: this.getFinalPropertyValue(formatJSON, 'direccion'),
            ejecutivo_cuenta: this.getFinalPropertyValue(formatJSON, 'ejecutivo_cuenta'),
            expediente: this.getFinalPropertyValue(formatJSON, 'expediente'),
            apertura_reciente: this.getFinalPropertyValue(formatJSON, 'apertura_reciente'),
            reposicion_chequera: this.getFinalPropertyValue(formatJSON, 'reposicion_chequera'),
            tipo_chequera: this.getFinalPropertyValue(formatJSON, 'tipo_chequera'),
            bolsillo: this.getFinalPropertyValue(formatJSON, 'bolsillo'),
            draft: this.getFinalPropertyValue(formatJSON, 'draft'),
            poliza_forma_superior: this.getFinalPropertyValue(formatJSON, 'poliza_forma_superior'),
            poliza_forma_inferior: this.getFinalPropertyValue(formatJSON, 'poliza_forma_inferior'),
            chequera_especial: this.getFinalPropertyValue(formatJSON, 'chequera_especial'),
            medidas_superior: this.getFinalPropertyValue(formatJSON, 'medidas_superior'),
            medidas_inferior: this.getFinalPropertyValue(formatJSON, 'medidas_inferior'),
            numero_chequeras: this.getFinalPropertyValue(formatJSON, 'numero_chequeras'),
            folio_inicial: this.getFinalPropertyValue(formatJSON, 'folio_inicial'),
            folio_final: this.getFinalPropertyValue(formatJSON, 'folio_final'),
            cantidad_cheques: this.getFinalPropertyValue(formatJSON, 'cantidad_cheques'),
            numero_proteccion: this.getFinalPropertyValue(formatJSON, 'numero_proteccion'),
            robo: this.getFinalPropertyValue(formatJSON, 'robo'),
            extravio: this.getFinalPropertyValue(formatJSON, 'extravio'),
            revocado: this.getFinalPropertyValue(formatJSON, 'revocado'),
            esqueletos_del: this.getFinalPropertyValue(formatJSON, 'esqueletos_del'),
            esqueletos_al: this.getFinalPropertyValue(formatJSON, 'esqueletos_al'),
            reportado_por: this.getFinalPropertyValue(formatJSON, 'reportado_por'),
            recibido_por: this.getFinalPropertyValue(formatJSON, 'recibido_por'),
            contrato: "",
            sucursal: this.getFinalPropertyValue(formatJSON, 'sucursal'),
            titular: this.getFinalPropertyValue(formatJSON, 'titular'),
            cotitular: "",
            no_cuenta: this.getFinalPropertyValue(formatJSON, 'no_cuenta'),
            correo: "",
            telefono: "",
            constancia: "",
            monto: "",
            plazo: "",
            tasa_minima: "",
            tasa_cupon: "",
            tasa_maxima: "",
            fecha_contratacion: "",
            fecha_vencimiento: "",
            instruccion_vencimiento: "",
            activo: "",
            nivel_a: "",
            nivel_b: "",
            fechas_observacion: "",
            funcionario: "",
            nombre_sucursal: this.getFinalPropertyValue(formatJSON, 'nombre_sucursal'),
            fecha_inicio: "",
            tasa_fija1: "",
            tasa_fija2: "",
            factor_a: "",
            factor_b: "",
            tipo_cambio_tcr: "",
            tipo_cambio_fix: ""
          },
          fechas:[  
            // {  
            //     fecha_determinacion: "4-Sep-18",
            //     fecha_inicio_periodo: "5-Sep-18",
            //     fecha_pago_interes: "3-Oct-18",
            //     periodo_interes: "28"
            // },
            // {  
            //     fecha_determinacion: "2-Oct-18",
            //     fecha_inicio_periodo: "3-Oct-18",
            //     fecha_pago_interes: "31-Oct-18",
            //     periodo_interes: "28"
            // },
            // {  
            //     fecha_determinacion: "30-Oct-18",
            //     fecha_inicio_periodo: "31-Oct-18",
            //     fecha_pago_interes: "28-Nov-18",
            //     periodo_interes: "28"
            // },
            // {  
            //     fecha_determinacion: "27-Nov-18",
            //     fecha_inicio_periodo: "28-Nov-18",
            //     fecha_pago_interes: "26-Dec-18",
            //     periodo_interes: "28"
            // },
            // {  
            //     fecha_determinacion: "24-Dec-18",
            //     fecha_inicio_periodo: "26-Dec-18",
            //     fecha_pago_interes: "23-Jan-18",
            //     periodo_interes: "28"
            // },
            // {  
            //     fecha_determinacion: "22-Jan-19",
            //     fecha_inicio_periodo: "23-Jan-19",
            //     fecha_pago_interes: "20-Feb-19",
            //     periodo_interes: "28"
            // },
            // {  
            //     fecha_determinacion: "19-Feb-19",
            //     fecha_inicio_periodo: "20-Feb-19",
            //     fecha_pago_interes: "20-Mar-19",
            //     periodo_interes: "28"
            // },
            // {  
            //     fecha_determinacion: "19-Mar-19",
            //     fecha_inicio_periodo: "20-Mar-19",
            //     fecha_pago_interes: "17-Apr-19",
            //     periodo_interes: "28"
            // },
            // {  
            //     fecha_determinacion: "16-Apr-19",
            //     fecha_inicio_periodo: "17-Apr-19",
            //     fecha_pago_interes: "15-May-19",
            //     periodo_interes: "28"
            // },
            // {  
            //     fecha_determinacion: "14-May-19",
            //     fecha_inicio_periodo: "15-May-19",
            //     fecha_pago_interes: "12-Jun-19",
            //     periodo_interes: "28"
            // },
            // {  
            //     fecha_determinacion: "11-Jun-19",
            //     fecha_inicio_periodo: "12-Jun-19",
            //     fecha_pago_interes: "10-Jul-19",
            //     periodo_interes: "28"
            // },
            // {  
            //     fecha_determinacion: "9-Jul-19",
            //     fecha_inicio_periodo: "10-Jul-19",
            //     fecha_pago_interes: "7-Aug-19",
            //     periodo_interes: "28"
            // },
            // {  
            //     fecha_determinacion: "6-Aug-19",
            //     fecha_inicio_periodo: "7-Aug-19",
            //     fecha_pago_interes: "4-Sep-19",
            //     periodo_interes: "28"
            // }
          ],
          tcrs:[  
            // {  
            //     tcr: "46000",
            //     rendimiento: "1.50"
            // },
            // {  
            //     tcr: "46500",
            //     rendimiento: "1.50"
            // },
            // {  
            //     tcr: "47000",
            //     rendimiento: "1.50"
            // },
            // {  
            //     tcr: "47500",
            //     rendimiento: "10.20"
            // },
            // {  
            //     tcr: "48000",
            //     rendimiento: "10.20"
            // },
            // {  
            //     tcr: "48500",
            //     rendimiento: "10.20"
            // },
            // {  
            //     tcr: "49000",
            //     rendimiento: "10.20"
            // },
            // {  
            //     tcr: "49500",
            //     rendimiento: "10.20"
            // },
            // {  
            //     tcr: "50000",
            //     rendimiento: "10.20"
            // },
            // {  
            //     tcr: "50500",
            //     rendimiento: "1.50"
            // },
            // {  
            //     tcr: "50500",
            //     rendimiento: "1.50"
            // },
            // {  
            //     tcr: "51000",
            //     rendimiento: "1.50"
            // },
            // {  
            //     tcr: "51500",
            //     rendimiento: "1.50"
            // }
          ],
          cheques: formatJSON['cheques'] != undefined ? formatJSON['cheques'] : [],
          type: formatJSON['formato'],
          typedoc: "PDF"
      }
    };

    return json;
  }

  getFinalPropertyValue(json, fieldName){
    return json[fieldName] != undefined ? json[fieldName] : '';
  }

  getPropertyValueX(json, fieldName, expectedValue){
    return json[fieldName] == expectedValue ? "X" : '';
  }

  getPropertyValue(json, fieldName){
    return json[fieldName] != undefined ? json[fieldName] : 'campo no informado/proporcionado por el cliente.';
  }

  downloadPDF(pdfInBase64, fileName) {

    if (window.navigator && window.navigator.msSaveOrOpenBlob) {
      // IE

      const pdfInBase64Decoded = atob(pdfInBase64);
      const byteNumbers = new Array(pdfInBase64Decoded.length);
      for (let i = 0; i < pdfInBase64Decoded.length; i++) {
        byteNumbers[i] = pdfInBase64Decoded.charCodeAt(i);
      }
      const byteArray = new Uint8Array(byteNumbers);
      const blob = new Blob([byteArray], { type: 'application/pdf' });
      window.navigator.msSaveOrOpenBlob(blob, fileName);

    } else {
      // Any other browser

      window.open('data:application/pdf;base64, ' + pdfInBase64, '', 'height=600,width=800');
    }
  }

}
