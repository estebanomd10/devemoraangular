export default interface CatalogOption {
    id: number | string,
    label: string,
    enabled?: boolean,
    selected?: boolean
};