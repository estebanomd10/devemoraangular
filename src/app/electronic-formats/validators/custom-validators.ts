import { FormControl, FormGroup } from '@angular/forms';

/*
  Custom validators.
*/

export function matchingNums(fromKey: string, untilKey: string) {
    return (group: FormGroup): { [key: string]: any } => {
        let from = group.controls[fromKey];
        let until = group.controls[untilKey];

        if (Number(until.value) <= Number(from.value)) {
            return {
                mismatchedNumbers: true
            };
        }
    }
}

