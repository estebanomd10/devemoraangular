import { Component, forwardRef, Input, OnInit } from '@angular/core';
import { ControlValueAccessor, NG_VALUE_ACCESSOR, Validator, NG_VALIDATORS } from '@angular/forms';

@Component({
  selector: 'app-data-grid',
  templateUrl: './data-grid.component.html',
  styleUrls: ['./data-grid.component.scss'],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => DataGridComponent),
      multi: true
    },
    { provide: NG_VALIDATORS, useExisting: DataGridComponent, multi: true }
  ]
})
export class DataGridComponent implements OnInit, ControlValueAccessor{

  @Input() config: {};

  // En este arreglo se almacenan todos los registros que dan de alta.
  records = [];

  ngOnInit(){
  
  }

  onChange: (value: any) => void;

  onTouched: any = () => { }

  /**
   * Set the function to be called 
   * when the control receives a change event.
   */
  registerOnChange(onChange: (value: any) => void) {
    this.onChange = onChange;
  }

  /**
   * Set the function to be called 
   * when the control receives a touch event.
   */
  registerOnTouched(onTouched: any): void {
    this.onTouched = onTouched;
  }

  /**
   * Write a new value to the element.
   */
  writeValue(value: any): void {
    this.records = value;
  }

  /**
   * Defines if the component has a valid state or not.
   * 
   * El control es válido sólo si han agregado, por lo menos, un registro.
   */
  validate() {

    if(this.records.length === 0) {
      return {
        'empty': 'Please, add at least one record to the table.'
      };
    }

    return null;
  }

  /**
   * Agrega un nuevo registro al arreglo.
   */
  addRecord(record: {}) {
    
    this.records.push(record);
    
    // Actualiza el value para el ControlValueAccessor.
    this.onChange(this.records); 
  }

  /**
   * Elimina el registro del index indicado.
   */
  deleteRecord(index: number) {

    let response = confirm("¿Estás seguro que deseas eliminar el registro?");
    if (response == true) {

      // Eliminamos el elemento del arreglo.
      this.records.splice(index, 1);

      // Actualiza el arreglo de records para el ControlValueAccessor.
      this.onChange(this.records);
    }
  }

}
