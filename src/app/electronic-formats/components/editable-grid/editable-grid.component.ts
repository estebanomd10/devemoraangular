import { Component, forwardRef, Input, OnInit } from '@angular/core';
import { ControlValueAccessor, NG_VALUE_ACCESSOR, Validator, NG_VALIDATORS } from '@angular/forms';

@Component({
  selector: 'app-editable-grid',
  templateUrl: './editable-grid.component.html',
  styleUrls: ['./editable-grid.component.scss'],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => EditableGridComponent),
      multi: true
    },
    { provide: NG_VALIDATORS, useExisting: EditableGridComponent, multi: true }
  ]
})
export class EditableGridComponent implements OnInit, ControlValueAccessor{

  @Input() config: {};

  // En este arreglo se almacenan todos los registros que dan de alta.
  records = [];

  ngOnInit(){
  
  }

  onChange: (value: any) => void;

  onTouched: any = () => { }

  /**
   * Set the function to be called 
   * when the control receives a change event.
   */
  registerOnChange(onChange: (value: any) => void) {
    this.onChange = onChange;
  }

  /**
   * Set the function to be called 
   * when the control receives a touch event.
   */
  registerOnTouched(onTouched: any): void {
    this.onTouched = onTouched;
  }

  /**
   * Write a new value to the element.
   */
  writeValue(value: any): void {
    this.records = value;
  }

  /**
   * Defines if the component has a valid state or not.
   * 
   * Este control es válido sólo si todos los campos de todos los registros son válidos,
   * ésto de acuerdo con las validaciones que fueron asignadas para cada campo definido en el json
   * de configuración que se pasó como parámetro de entrada.
   */
  validate() {

    // Por default el control es válido y cambia a inválido sólo si encuentra un registro inválido en el ciclo.
    let recordsIsValid = true;

    for(let record of this.records){
      
      // Por default cada registro es válido y cambia a inválido sólo si encuentra un campo inválido en el ciclo.
      record['valid'] = true;

      for(let field of this.config['fields']) {
        
        // Al aplicar las validaciones asignadas sabemos si el campo es válido o inválido.
        field['valid'] = this.applyFieldValidation(field, record[field['name']]);

        record['valid'] = record['valid'] && field['valid']; 
      }

      recordsIsValid = recordsIsValid && record['valid'];
    }

    if(!recordsIsValid) {
      return {
        'invalidFields': 'Please, review all records fields have a valid data.'
      };
    }

    return null;
  }

  /**
   * Aplica la validación correspondiente al campo que se pasa como parámetro.
   * @param field: json con los attributos asignados al campo
   * @param recordFieldValue: Valor de campo que vamos a verificar de acuerdo con las validaciones asignadas.
   */
  applyFieldValidation(field: {}, recordFieldValue: any) {

    let isFieldValid = false;

    // Valida si el campo es requerido o no.
    if(field['required'] != undefined) {
      switch(field['required']) {
        case true:
          isFieldValid = (recordFieldValue != undefined) && (recordFieldValue != null) && (recordFieldValue != '');
          break;

        case false:
          isFieldValid = true;
          break;
      }
    }

    // Si se asignó un patrón al campo, validamos que lo cumpla.
    if(field['pattern'] != undefined) {
      isFieldValid = isFieldValid && (new RegExp(field['pattern'])).test(recordFieldValue);
    }
    
    return isFieldValid;
  }

  /**
   * Agrega un nuevo registro al arreglo.
   */
  addRecord() {
    
    this.records.push({});
    
    // Actualiza el value para el ControlValueAccessor.
    this.onChange(this.records); 
  }

  /**
   * Elimina el registro del index indicado.
   */
  deleteRecord(index: number) {

    let response = confirm("¿Estás seguro que deseas eliminar el registro?");
    if (response == true) {

      // Eliminamos el elemento del arreglo.
      this.records.splice(index, 1);

      // Actualiza el arreglo de records para el ControlValueAccessor.
      this.onChange(this.records);
    }
  }

}
