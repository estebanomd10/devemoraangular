import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GetCustomerDataComponent } from './get-customer-data.component';

describe('GetCustomerDataComponent', () => {
  let component: GetCustomerDataComponent;
  let fixture: ComponentFixture<GetCustomerDataComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GetCustomerDataComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GetCustomerDataComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
