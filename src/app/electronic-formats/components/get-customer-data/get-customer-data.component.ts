import { Component, OnInit, ViewChild, ElementRef, Output, EventEmitter } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
//import { CatalogDataService } from '../../../services/catalogsDataService';
import { HttpErrorResponse } from '@angular/common/http';
//import { GlobalFunctionsService } from '../../services/global-functions.service';

@Component({
 selector: 'app-get-customer-data',
 templateUrl: './get-customer-data.component.html',
 styleUrls: ['./get-customer-data.component.scss']
})
export class GetCustomerDataComponent implements OnInit {

 @ViewChild("numCuentaCliente") inputNumCuentaCliente: ElementRef;
 @Output() showNextStep: EventEmitter<any> = new EventEmitter<any>();

 frmNumCuentaCliente: FormGroup;
 customerData = {};
 displayDatosCliente = false;

 error = '';
 displayError = false;
//private _CatalogDataService: CatalogDataService, private globalFunctions: GlobalFunctionsService
 constructor(private fb: FormBuilder  ) {}

 ngOnInit() {
   this.frmNumCuentaCliente = this.fb.group({
     'numCuentaCliente': [null, Validators.compose([Validators.required, Validators.pattern('^[0-9]*$'), Validators.minLength(11), Validators.maxLength(11)])]
   });
 }

 validateNumCuentaCliente() {

   if(this.frmNumCuentaCliente.valid){

     // Reseteamos las banderas y mensaje de error.
     this.displayDatosCliente = true;
     this.error = '';
     this.displayError = false;

     let numCuenta = this.frmNumCuentaCliente.value.numCuentaCliente;

     this.customerData['numCuenta'] = numCuenta;
           this.customerData['buc'] = "000000";
           this.customerData['nombre'] = "nombre1";

    /* this._CatalogDataService.validateNumCuentaCliente(numCuenta).subscribe(

       res => {
         console.info("TRANSACTION RESPONSE");
         console.info(res);

         let status = res['statusDesc'];

         if(status == "SUCCESS"){

           this.customerData['numCuenta'] = res['numCuenta'];
           this.customerData['buc'] = res['numCliente'];
           this.customerData['nombre'] = res['nombreCliente'];

           this.displayDatosCliente = true;

         } else {

           // ERROR
           this.error = res['addStatusDesc'];

           this.displayError = true;
         }

         console.info("END TRANSACTION RESPONSE");
       },
       (err: HttpErrorResponse) => {
         console.info("TRANSACTION ERRORERRORERRORERRORERRORERRORERRORERRORERROR");
         console.info(err);

         if (err.status >= 500) {
           console.info("Server-side error occured: [" + err.status + "]");
         } else {
           console.info("Client-side error occured: [" + err.status + "]");
         }
         console.info("TRANSACTION ERRORERRORERRORERRORERRORERRORERRORERRORERROR");
       }
     );*/
   }
 }

 consultarOtraCuenta() {
   // Reseteamos las banderas y mensaje de error.
   this.displayDatosCliente = false;
   this.error = '';
   this.displayError = false;

   this.frmNumCuentaCliente.controls['numCuentaCliente'].setValue(null);
   this.inputNumCuentaCliente.nativeElement.focus();
 }

 emitShowNextStep() {
   console.info("Emiting Next Step Event: " + JSON.stringify(this.customerData));
   this.showNextStep.emit(this.customerData);
 }



}