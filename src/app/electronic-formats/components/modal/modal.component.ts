import { Component, OnInit } from '@angular/core';
import { GlobalFunctionsService } from './../../services/global-functions.service';

declare let $: any;

@Component({
  selector: 'app-modal',
  templateUrl: './modal.component.html',
  styleUrls: ['./modal.component.scss']
})
export class ModalComponent implements OnInit {

  pregunta = '¿Estás seguro que deseas cancelar este formato?';

  constructor(private globalFunctions: GlobalFunctionsService) { }

  ngOnInit() {
  }

  cancelFormat() {
    $('#myModal').modal('hide');
    this.globalFunctions.gotoHome();
  }

}
