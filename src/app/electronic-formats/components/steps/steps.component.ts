import { Component } from '@angular/core';

@Component({
  selector: 'app-steps',
  templateUrl: './steps.component.html',
  styleUrls: ['./steps.component.scss']
})
export class StepsComponent {
  
  step1_passed = false;
  step2_passed = false;
  step3_passed = false;

  passedSteps = [false, false, false];
  
  markStepAsPassed(step) {
    this.passedSteps[step - 1] = true;
  }
  
}
