import * as console from 'console';
import { Component, Input, Output, forwardRef, EventEmitter } from '@angular/core';
import { ControlValueAccessor, NG_VALUE_ACCESSOR, Validator, NG_VALIDATORS } from '@angular/forms';

@Component({
  selector: 'app-radio-group',
  templateUrl: './radio-group.component.html',
  styleUrls: ['./radio-group.component.scss'],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => RadioGroupComponent),
      multi: true
    },
    { provide: NG_VALIDATORS, useExisting: RadioGroupComponent, multi: true }
  ]
})
export class RadioGroupComponent implements ControlValueAccessor { 

  @Input() options: [{id: number | string, label: string , enabled?: boolean, selected?: boolean }];
  @Input() groupName: string;
  @Output() selectionChanged: EventEmitter<{}> = new EventEmitter<{}>();


  selectedOption: {};

  onChange: (value: {}) => void;

  onTouched: any = () => { }

  /**
   * Set the function to be called 
   * when the control receives a change event.
   */
  registerOnChange(onChange: (value: {}) => void) {
    this.onChange = onChange;
  }

  /**
   * Set the function to be called 
   * when the control receives a touch event.
   */
  registerOnTouched(onTouched: any): void {
    this.onTouched = onTouched;
  }

  /**
   * Write a new value to the element.
   */
  writeValue(value: {}): void {
    this.selectedOption = value != undefined ? value : {};
  }

  /**
   * Defines if the component has a valid state or not.
   */
  validate() {

    if((this.selectedOption['id'] === undefined) || (this.selectedOption['id'] === 0)){
      return {
        'required': 'Please choose one option.'
      };
    }
   
    return null;
  }

  changeSelected(id, label) {

    this.selectedOption = {
      id: id,
      label: label
    };
    this.onChange(this.selectedOption);

    this.selectionChanged.emit(this.selectedOption);
   }
}
