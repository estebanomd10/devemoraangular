import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EfSidebarComponent } from './ef-sidebar.component';

describe('EfSidebarComponent', () => {
  let component: EfSidebarComponent;
  let fixture: ComponentFixture<EfSidebarComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EfSidebarComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EfSidebarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
