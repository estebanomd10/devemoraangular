import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-ef-sidebar',
  templateUrl: './ef-sidebar.component.html',
  styleUrls: ['./ef-sidebar.component.scss']
})
export class EfSidebarComponent implements OnInit {

  @Input() customerData: any;

  constructor() { }

  ngOnInit() {
  }

}
