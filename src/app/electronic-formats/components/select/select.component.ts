import { Component, Input, Output, forwardRef, EventEmitter } from '@angular/core';
import { ControlValueAccessor, NG_VALUE_ACCESSOR, Validator, NG_VALIDATORS } from '@angular/forms';


@Component({
  selector: 'app-select',
  templateUrl: './select.component.html',
  styleUrls: ['./select.component.scss'],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => SelectComponent),
      multi: true
    },
    { provide: NG_VALIDATORS, useExisting: SelectComponent, multi: true }
  ]
})
export class SelectComponent implements ControlValueAccessor {

  @Input() options: [{id: number | string, label: string , enabled?: boolean, selected?: boolean }];
  @Output() selectionChanged: EventEmitter<{}> = new EventEmitter<{}>();

  selectedOption: {};

  onChange: (value: {}) => void;

  onTouched: any = () => { };

  /**
   * Set the function to be called 
   * when the control receives a change event.
   */
  registerOnChange(onChange: (value: {}) => void) {
    this.onChange = onChange;
  }

  /**
   * Set the function to be called 
   * when the control receives a touch event.
   */
  registerOnTouched(onTouched: any): void {
    this.onTouched = onTouched;
  }

  /**
   * Write a new value to the element.
   */
  writeValue(value: {}): void {
    this.selectedOption = value != undefined ? value : {};
  }

  /**
   * Defines if the component has a valid state or not.
   */
  validate() {

    if((this.selectedOption['id'] === undefined) || (this.selectedOption['id'] === 0)){
      return {
        'required': 'Please choose one option.'
      };
    }

    return null;
  }

  changeSelected(event) {

    this.selectedOption = {
      id: event.target.value,
      label: event.target.options[event.target.options.selectedIndex].label
    };
    this.onChange(this.selectedOption);

    this.selectionChanged.emit(this.selectedOption);
  }
}
