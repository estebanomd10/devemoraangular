import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-substeps',
  templateUrl: './substeps.component.html',
  styleUrls: ['./substeps.component.scss']
})
export class SubstepsComponent implements OnInit {

  @Input() stepsNumber: number;

  steps = [];
  currentStep = 1;
 
  /**
   * Starts the array of steps according to the stepsNumber passed as input to the component and shows the first step.
   */
  ngOnInit() {

    for(let i=0 ; i < this.stepsNumber ; i++) {
      this.steps.push({});
      this.steps[i]['number'] = i + 1;
      this.steps[i]['enabled'] = false;
      this.steps[i]['passed'] = false;
    }

    this.showStep(1);
  }

  /**
   * Enables the step passed as parameter, and shows its content.
   * @param step: from 1 to stepsNumber.
   * @param fromClickOnIcon: It helps us to know if the function was called from clicking the icon or directly, in order to know when to show the step content. 
   */
  showStep(step: number, fromClickOnIcon?: boolean) {

    if(fromClickOnIcon){

      if(this.steps[step - 1]['enabled']) {
        this.currentStep = step;
      }
      
    } else {

      this.enableStep(step);
      this.currentStep = step;
    }
  }

  /**
   * Set to true the 'passed' flag of the step passed as parameter.
   * @param step: from 1 to stepsNumber.
   */
  markStepAsPassed(step: number) {
    this.steps[step - 1]['passed'] = true;
  }

  /**
   * Set to true the 'enabled' flag of the step passed as parameter.
   * @param step: from 1 to stepsNumber.
   */
  enableStep(step: number) {
    this.steps[step - 1]['enabled'] = true;
  }

}
