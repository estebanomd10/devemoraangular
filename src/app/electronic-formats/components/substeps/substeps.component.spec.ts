import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SubstepsComponent } from './substeps.component';

describe('SubstepsComponent', () => {
  let component: SubstepsComponent;
  let fixture: ComponentFixture<SubstepsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SubstepsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SubstepsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
