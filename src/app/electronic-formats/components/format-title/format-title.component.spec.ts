import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FormatTitleComponent } from './format-title.component';

describe('FormatTitleComponent', () => {
  let component: FormatTitleComponent;
  let fixture: ComponentFixture<FormatTitleComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FormatTitleComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FormatTitleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
