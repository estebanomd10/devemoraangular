import { Component, Input } from '@angular/core';

@Component({
  selector: 'app-format-title',
  templateUrl: './format-title.component.html',
  styleUrls: ['./format-title.component.scss']
})
export class FormatTitleComponent {

  @Input() title: string;
  @Input() subtitle: string;
  
}
