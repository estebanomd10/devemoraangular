import { Component, EventEmitter, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { StepsComponent } from '../../components/steps/steps.component';
import { SubstepsComponent } from '../../components/substeps/substeps.component';


@Component({
 selector: 'app-bhf-003',
 templateUrl: './bhf-003.component.html',
 styleUrls: ['./bhf-003.component.scss']
})

export class Bhf003Component {

 @ViewChild(StepsComponent) steps:StepsComponent;
 @ViewChild(SubstepsComponent) substeps:SubstepsComponent;

 // Los datos del cliente los vamos a obtener en el paso 1.
 form_1: FormGroup;

 customerData = {};

 esClienteOptions = [
   {id: 1, label: "Sí"},
   {id: 2, label: "No",}
 ];

 isMostrarBucCliente: boolean = false;
 isNuevosDatosCliente: boolean = false;

 // Los datos de los cheques los vamos a obtener en el paso 2.
 checksData = {};

 displayStep_1 = true;
 displayStep_2 = false;

 showModal: EventEmitter<boolean> = new EventEmitter();
 constructor(private fb: FormBuilder){ }

 ngOnInit() {

         this.form_1 = this.fb.group({
           'usar_SPID': [{}, Validators.required],
           'firma_electronica': [null, Validators.required]
         });
       }

 selectionChangedForEsCliente(value) {
   this.isMostrarBucCliente = (value['id'] === 1) ? true: false;
   this.isNuevosDatosCliente = (value['id'] === 1) ? false: true;
 }

 onShowNextStep(event) {
   //Aquí obtenemos los datos del cliente.
   this.customerData = event;
  
   this.displayStep_1 = false;
   this.displayStep_2 = true;
   this.steps.markStepAsPassed(1);
 }

 onShowPreviewPDF(event) {

   alert("calling print PDF.");
   // Aquí obtenemos todos los datos de los cheques.
   // this.checksData = event;

   // this.showModal.emit(true);
   // this.steps.markStepAsPassed(2);
   // this.steps.markStepAsPassed(3);
 }

 substepPassed(stepNumber: number) {
   this.substeps.markStepAsPassed(stepNumber);
 }

 showStep(stepNumber: number) {
   this.substeps.showStep(stepNumber);
 }


}