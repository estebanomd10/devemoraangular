import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Bhf003Component } from './bhf-003.component';

describe('Bhf003Component', () => {
  let component: Bhf003Component;
  let fixture: ComponentFixture<Bhf003Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Bhf003Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Bhf003Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
