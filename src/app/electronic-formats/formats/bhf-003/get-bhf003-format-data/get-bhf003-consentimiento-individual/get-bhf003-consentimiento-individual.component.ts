import { Component, OnInit, ViewChild, ElementRef, Output, EventEmitter, Input } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
//import { CatalogDataService } from '../../../../services/catalogsDataService';
import { HttpErrorResponse } from '@angular/common/http';
//import { GlobalFunctionsService } from '../../../services/global-functions.service';
import { matchingNums } from '../../../../validators/custom-validators';


@Component({
  selector: 'app-get-bhf003-consentimiento-individual',
  templateUrl: './get-bhf003-consentimiento-individual.component.html',
  styleUrls: ['./get-bhf003-consentimiento-individual.component.css','./get-bhf003-consentimiento-individual.component.scss']
})
export class GetBhf003ConsentimientoIndividualComponent implements OnInit {
  @Output() showIndividualConsent: EventEmitter<any> = new EventEmitter<any>();
  @Input() customerData: any;
  customerDatos = {};

  form_1: FormGroup;
  confirmation_options = [{id: 1, label: 'SI'}, {id: 2, label: 'NO'} ];
  disease_option = [{id: 1, label: 'Problemas vasculares (trombosis, embolia, hemorragia, aneurisma u otros), Infarto al miocardio o Insuficiencia coronaria, Insuficiencia renal'}, 
                    {id: 2, label: 'Cáncer o tumor de cualquier clase, SIDA, o se le ha dicho que es portador del Síndrome de Inmunodeficiencia Adquirida'},
                    {id: 3, label: 'Parálisis, Ceguera o Sordera total'},
                    {id: 4, label: 'Epilepsia o cualquier otra enfermedad neurológica'},
                    {id: 5, label: 'En los últimos 5 años, ¿Ha consumido alcohol en exceso hasta la intoxicación, drogas sin prescripción médica?'},
                    {id: 6, label: 'Padecimiento de las glándulas, diabetes'},
                    {id: 7, label: 'Problemas relacionados con el corazón, aparato circulatorio, hipertensión arterial'},
                    {id: 8, label: 'Alteraciones del sentido de las vista o el oído'},
                    {id: 9, label: 'Enfermedades de los pulmones, hígado o páncreas'},
                    {id: 10, label: 'Enfermedades de aparato digestivo o genitourinario, sistema músculo esquelético'},
                    {id: 11, label: 'En los últimos 5 años ¿ha estado Ud. Internado en un hospital u otra institución de salud o de cualquier manera incapacitado para desempeñar sus actividades normales de trabajo?'},
                    {id: 12, label: '¿Viaja en aeronaves que no pertenecen a líneas comerciales de aviación?'},
                    {id: 13, label: '¿Ud. fuma?'},
                    {id: 14, label: '¿Tiene Ud. Otros seguros de vida con Zurich Santander Seguros México, S.A.?'}];

  constructor(private fb: FormBuilder) { }

  ngOnInit() {
    this.customerDatos = this.customerData;

    this.form_1 = this.fb.group({
      'peso':[null, [Validators.required, Validators.pattern('^[a-zA-Z0-9 -\/ \sáéíóúñÁÉÍÓÚÑ]+$'), Validators.maxLength(10)]],
      'estatura':  [null, [Validators.required, Validators.pattern('^[a-zA-Z0-9 -\/ \sáéíóúñÁÉÍÓÚÑ]+$'), Validators.maxLength(10)]],
      'option1': [{},Validators.required],
      'option2': [{},Validators.required],
      'option3': [{},Validators.required],
      'option4': [{},Validators.required],
      'option5': [{},Validators.required],
      'option6': [{},Validators.required],
      'option7': [{},Validators.required],
      'option8': [{},Validators.required],
      'option9': [{},Validators.required],
      'option10': [{},Validators.required],
      'option11': [{},Validators.required],
      'option12': [{},Validators.required],
      'option13': [{},Validators.required],
      'option14': [{},Validators.required],
    });
  }

}
