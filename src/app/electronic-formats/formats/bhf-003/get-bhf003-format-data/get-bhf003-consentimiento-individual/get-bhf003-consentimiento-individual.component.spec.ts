import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GetBhf003ConsentimientoIndividualComponent } from './get-bhf003-consentimiento-individual.component';

describe('GetBhf003ConsentimientoIndividualComponent', () => {
  let component: GetBhf003ConsentimientoIndividualComponent;
  let fixture: ComponentFixture<GetBhf003ConsentimientoIndividualComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GetBhf003ConsentimientoIndividualComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GetBhf003ConsentimientoIndividualComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
