import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GetBhf003InformacionEconomicaComponent } from './get-bhf003-informacion-economica.component';

describe('GetBhf003InformacionEconomicaComponent', () => {
  let component: GetBhf003InformacionEconomicaComponent;
  let fixture: ComponentFixture<GetBhf003InformacionEconomicaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GetBhf003InformacionEconomicaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GetBhf003InformacionEconomicaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
