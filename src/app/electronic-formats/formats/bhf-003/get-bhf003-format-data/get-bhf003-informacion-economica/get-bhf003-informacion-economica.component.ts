import { Component, OnInit, ViewChild, ElementRef, Output, EventEmitter, Input } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
//import { CatalogDataService } from '../../../../services/catalogsDataService';
import { HttpErrorResponse } from '@angular/common/http';
//import { GlobalFunctionsService } from '../../../services/global-functions.service';

import { matchingNums } from '../../../../validators/custom-validators';

import { SubstepsComponent } from '../../../../components/substeps/substeps.component';
import { SelectComponent } from '../../../../components/select/select.component';

@Component({
  selector: 'app-get-bhf003-informacion-economica',
  templateUrl: './get-bhf003-informacion-economica.component.html',
  styleUrls: ['./get-bhf003-informacion-economica.component.scss']
})
export class GetBhf003InformacionEconomicaComponent implements OnInit {
  @Output() showEconomicInformation: EventEmitter<any> = new EventEmitter<any>();
  @Input() customerData: any;
  customerDatos = {};
  @ViewChild(SubstepsComponent) substeps:SubstepsComponent;

  cuentaConCreditosOptions = [{id: 1, label: 'SI'}, {id: 2, label: 'NO'} ];
  tarjetaOptions = [ {id: 1, label: 'Tarjeta 1'}, {id: 2, label: 'Tarjeta 2'} ];
  // En este arreglo se almacenan todos los registros de beneficiarios que den de alta.
  antecedentesCredito = [];
  // Datos de configuración del editable-grid para beneficiarios. 
  config_antecedentesCredito = {
    title: 'Antecedentes y Referencias de Crédito',
    fields: [
      {
        name: 'institucion',
        type: 'input',
        label: 'Institución o Entidad',
        required: true,
      },
      {
        name: 'tarjeta_credito',
        type: 'select',
        label: 'Tipo de producto',
        required: true,
        options: this.tarjetaOptions
      },
      {
        name: 'num_cuenta',
        type: 'input',
        label: 'No. Cuenta',
        required: true,
      },
      {
        name: 'saldo_actual',
        type: 'input',
        label: 'Saldo actual',
        required: true,
      },
      {
        name: 'pago_mensual',
        type: 'input',
        label: 'Pago mensual',
        required: true,
      }    
    ]
  };

  form_1: FormGroup;
  displayInput_1=false;
//private catalogDataService: CatalogDataService  private globalFunctions: GlobalFunctionsService
  constructor(private fb: FormBuilder) { }

  ngOnInit() {
    this.customerDatos = this.customerData;

    this.form_1 = this.fb.group({
      'cuenta_con_creditos': [[], Validators.compose([Validators.required])],
      'antecedentes_credito': [[], Validators.compose([Validators.required])]
    });
  }

  selectionChangedForCuentaCredito(){
     this.displayInput_1=true;
  }

  submit_form_1() {
    console.info("submit_form_4 " + JSON.stringify(this.customerDatos));
    this.showEconomicInformation.emit(this.customerDatos);
  }

  cancelFormat() {
    let response = confirm("¿Estás seguro que deseas cancelar este formato?");
    if (response == true) {
      //this.globalFunctions.gotoHome();
    }
  }

  gotoHome() {
    //this.globalFunctions.gotoHome();
  }

}
