import { Component, OnInit, ViewChild, ElementRef, Output, EventEmitter, Input } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
//import { CatalogDataService } from '../../../../services/catalogsDataService';
import { HttpErrorResponse } from '@angular/common/http';
//import { GlobalFunctionsService } from '../../../services/global-functions.service';

import { matchingNums } from '../../../../validators/custom-validators';

import { SubstepsComponent } from '../../../../components/substeps/substeps.component';
import { SelectComponent } from '../../../../components/select/select.component';

@Component({
  selector: 'app-get-bhf003-datos-personales-solicitante',
  templateUrl: './get-bhf003-datos-personales-solicitante.component.html',
  styleUrls: ['./get-bhf003-datos-personales-solicitante.component.css','./get-bhf003-datos-personales-solicitante.component.scss']
})
export class GetBhf003DatosPersonalesSolicitanteComponent implements OnInit {
  @Output() showPersonalInformation: EventEmitter<any> = new EventEmitter<any>();
  @Input() customerData: any;
  customerDatos = {};
  @ViewChild(SubstepsComponent) substeps:SubstepsComponent;

  form_1: FormGroup;
  sexo_options =[ {id: 1, label: "Masculino"}, {id: 2, label: "Femenino"}];
  pais_options=[ {id: 1, label: "México"}, {id: 2, label: "Estados Unidos"}];
  entidad_options= [ {id: 1, label: "Morelos"}, {id: 2, label: "Aguascalientes"}];
  nacionalidad_options =[ {id: 1, label: "Mexicana"}, {id: 2, label: "Estadounidense"}];

  form_2: FormGroup;
  identificacion_options=[ {id: 1, label: "INE"}, {id: 2, label: "Pasaporte"}];
  escolaridad_options=[ {id: 1, label: "Licenciatura"}, {id: 2, label: "Doctorado"}];

  form_3: FormGroup;
  colonia_options =[ {id: 1, label: "Lomas de guadalupe"}, {id: 2, label: "Lomas del carril"}];

  form_4: FormGroup;
  vive_casa_options =[ {id: 1, label: "Propia"}, {id: 2, label: "Rentada"}];
  displayInputs_1 = false;
  displayInputs_2 = false;
  confirmation_options = [ {id: 1, label: "Sí"},{id: 2, label: "No"}];
  estado_civil_options = [ {id: 1, label: "Divorciado"},{id: 2, label: "Casado"}];

  constructor(private fb: FormBuilder) { }

  ngOnInit() {
    this.customerDatos = this.customerData;
    
    this.form_1 = this.fb.group({
      'nombre': [null, [Validators.required, Validators.pattern('^[a-zA-Z0-9 -\/ \sáéíóúñÁÉÍÓÚÑ]+$'), Validators.maxLength(22)]],
      'apellido_paterno': [null, [Validators.required, Validators.pattern('^[a-zA-Z0-9 -\/ \sáéíóúñÁÉÍÓÚÑ]+$'), Validators.maxLength(22)]],
      'apellido_materno': [null, [Validators.required, Validators.pattern('^[a-zA-Z0-9 -\/ \sáéíóúñÁÉÍÓÚÑ]+$'), Validators.maxLength(22)]],
      'fechaNacimiento':[null, Validators.required],
      'sexo': [null, Validators.required],
      'correo': [null, [Validators.required, Validators.pattern('[áéíóúñÁÉÍÓÚÑA-Za-z0-9._%+-]+@[áéíóúñÁÉÍÓÚÑA-Za-z0-9.-]+\.[áéíóúñÁÉÍÓÚÑA-Za-z]{2,3}$')]],
      'pais': [null, Validators.required],
      'entidad': [null, Validators.required],
      'nacionalidad': [null, Validators.required]
    });

    this.form_2 = this.fb.group({
      'identificacion': [null, Validators.required],
      'numeroIdentificacion': [null, [Validators.required, Validators.pattern('^[0-9]*$'), Validators.maxLength(13)]],
      'rfc': [null, [Validators.required, Validators.pattern('^[a-zA-Z0-9 ]+$'), Validators.maxLength(13)]],
      'curp': [null, [Validators.required, Validators.pattern('^[a-zA-Z0-9 ]+$'), Validators.maxLength(18)]],
      'numeroSeguro': [null, [Validators.required, Validators.pattern('^[0-9]*$'), Validators.maxLength(11)]],
      'escolaridad': [null, Validators.required],
      'profesion': [null, [Validators.required, Validators.pattern('^[a-zA-Z0-9 -\/ \sáéíóúñÁÉÍÓÚÑ]+$'), Validators.maxLength(18)]]       
    });

    this.form_3 = this.fb.group({
      'calle': [null, [Validators.required, Validators.pattern('^[a-zA-Z0-9 ]+$'), Validators.maxLength(22)]],
      'numeroExterior': [null, [Validators.required, Validators.pattern('^[0-9]*$'), Validators.maxLength(4)]], 
      'numeroInterior': [null, [Validators.required, Validators.pattern('^[0-9]*$'), Validators.maxLength(4)]],
      'cp': [null, [Validators.required, Validators.pattern('^[0-9]*$'), Validators.maxLength(4)]],
      'colonia':  [null, Validators.required],  
      'estado': [null, Validators.required], 
      'delegacion': [null, [Validators.required, Validators.pattern('^[a-zA-Z0-9 ]+$'), Validators.maxLength(22)]], 
      'paiss': [null, Validators.required],   
      'telCasa': [null, [Validators.required, Validators.pattern('^[0-9]*$'), Validators.maxLength(10)]],
      'telCelular': [null, [Validators.required, Validators.pattern('^[0-9]*$'), Validators.maxLength(10)]],
    });
    
    this.form_4 = this.fb.group({
      'viveCasa': [null, Validators.required],
      'conHipoteca': [{}],
      'tiempoResidencia': [null],
      'estadoCivil': [null, Validators.required],
      'pagaPension': [{}],
      'importe': [null],
      'depenEconomicos': [null]
    });
  }

  cancelFormat() {
    let response = confirm("¿Estás seguro que deseas cancelar este formato?");
    if (response == true) {
     // this.globalFunctions.gotoHome();
    }
  }

  submit_form_1(){
    this.substeps.showStep(2);
  }

  submit_form_2(){
    this.substeps.showStep(3);
  }

  submit_form_3(){
    this.substeps.showStep(4);
  }

  submit_form_4(){
    console.info("submit_form_4 " + JSON.stringify(this.customerDatos));
    this.showPersonalInformation.emit(this.customerDatos);
  }

  selectionChangedForViveCasas(selectedOption: {}) {
       this.displayInputs_1 = true;
       this.form_4.controls['conHipoteca'].setValidators(Validators.required);
       this.form_4.controls['tiempoResidencia'].setValidators([Validators.required,Validators.pattern('^[a-zA-Z0-9 -\/ \sáéíóúñÁÉÍÓÚÑ]+$'), Validators.maxLength(10)]);        
  }

  selectionChangedForEstadoCivil(selectedOption: {}) {
    this.displayInputs_2 = true;
    this.form_4.controls['pagaPension'].setValidators(Validators.required);
    this.form_4.controls['importe'].setValidators([Validators.required,Validators.pattern('^[0-9]*$'), Validators.maxLength(10)]);
    this.form_4.controls['depenEconomicos'].setValidators([Validators.required,Validators.pattern('^[0-9]*$'), Validators.maxLength(2)]);  
}

  goToStep (){}

}