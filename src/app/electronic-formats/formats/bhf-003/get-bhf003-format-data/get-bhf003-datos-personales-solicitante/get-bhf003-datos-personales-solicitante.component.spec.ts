import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GetBhf003DatosPersonalesSolicitanteComponent } from './get-bhf003-datos-personales-solicitante.component';

describe('GetBhf003DatosPersonalesSolicitanteComponent', () => {
  let component: GetBhf003DatosPersonalesSolicitanteComponent;
  let fixture: ComponentFixture<GetBhf003DatosPersonalesSolicitanteComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GetBhf003DatosPersonalesSolicitanteComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GetBhf003DatosPersonalesSolicitanteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
