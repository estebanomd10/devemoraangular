import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GetBhf003PatrimonioComponent } from './get-bhf003-patrimonio.component';

describe('GetBhf003PatrimonioComponent', () => {
  let component: GetBhf003PatrimonioComponent;
  let fixture: ComponentFixture<GetBhf003PatrimonioComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GetBhf003PatrimonioComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GetBhf003PatrimonioComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
