import { Component, OnInit, ViewChild, ElementRef, Output, EventEmitter, Input } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
//import { CatalogDataService } from '../../../../services/catalogsDataService';
import { HttpErrorResponse } from '@angular/common/http';
//import { GlobalFunctionsService } from '../../../services/global-functions.service';

import { matchingNums } from '../../../../validators/custom-validators';


@Component({
  selector: 'app-get-bhf003-patrimonio',
  templateUrl: './get-bhf003-patrimonio.component.html',
  styleUrls: ['./get-bhf003-patrimonio.component.css','./get-bhf003-patrimonio.component.scss']
})
export class GetBhf003PatrimonioComponent implements OnInit {
  @Output() showHeritage: EventEmitter<any> = new EventEmitter<any>();
  @Input() customerData: any;
  customerDatos = {};

  form_1: FormGroup;
  cheques = [];
  countImports=0;
  countValor=0;

  checks_imports_options=[{id: 1, check:false,label: "Saldo en cuenta de cheques en Santander",     input:false, import:""}, 
                  {id: 2, check:false, label: "Saldo en cuenta de cheques en otros bancos", input:false, import:""},
                  {id: 3, check:false, label: "Saldo en inversiones en Santander",          input:false, import:""},
                  {id: 4, check:false, label: "Saldo en inversiones en otros bancos",       input:false, import:""}];

  checks_valor_options=[{id: 1, check:false,label: "Vivienda actual",                  input:false, import:""}, 
                        {id: 2, check:false, label: "Otros inmuebles de su propiedad", input:false, import:""},
                        {id: 3, check:false, label: "Automóvil(es)",                   input:false, import:""},
                        {id: 4, check:false, label: "Otros bienes nuebles",            input:false, import:""}];

  constructor(private fb: FormBuilder) { }

  ngOnInit() {
      this.customerDatos = this.customerData;

       this.form_1 = this.fb.group({
        'cheques': [[], Validators.compose([Validators.required])]
      });
  }

  checkboxOptionImports(value: boolean, checks, index) {
  if(value == true){this.countImports ++; }else{this.countImports --;}
    for (var x = 0; x < this.checks_imports_options.length; x++) {
      if (this.checks_imports_options[x].id === checks.id) {
          this.checks_imports_options[x].check = value;
          this.checks_imports_options[x].input = value;
          this.checks_imports_options[x].import = "";
      }
    }
  }

  checkboxOptionValor(value: boolean, checks, index) {
    if(value == true){this.countValor ++; }else{this.countValor --;}
      for (var x = 0; x < this.checks_valor_options.length; x++) {
        if (this.checks_valor_options[x].id === checks.id) {
            this.checks_valor_options[x].check = value;
            this.checks_valor_options[x].input = value;
            this.checks_valor_options[x].import = "";
        }
      }
    }

    submit_form_1() {
      console.info("submit_form_5  " + JSON.stringify(this.customerDatos));
      this.showHeritage.emit(this.customerDatos);
    }

}
