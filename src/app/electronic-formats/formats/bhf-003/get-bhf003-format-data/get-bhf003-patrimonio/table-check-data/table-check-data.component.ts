import { Component, forwardRef } from '@angular/core';
import { ControlValueAccessor, NG_VALUE_ACCESSOR, Validator, NG_VALIDATORS } from '@angular/forms';

declare let $: any;

@Component({
  selector: 'app-table-check-data',
  templateUrl: './table-check-data.component.html',
  styleUrls: ['./table-check-data.component.scss'],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => TableCheckDataComponent),
      multi: true
    },
    { provide: NG_VALIDATORS, useExisting: TableCheckDataComponent, multi: true }
  ]
})
export class TableCheckDataComponent implements ControlValueAccessor{

  // En este arreglo se almacenan todos los datos de los cheques que dan de alta.
  
  checks=[ {id: 1, label: "Saldo en cuenta de cheques en Santander", price:"10,000.00"}, 
                      {id: 2, label: "Saldo en cuenta de cheques en otros bancos", price:""},
                      {id: 3, label: "Saldo en inversiones en Santander", price:"8,000.00"},
                      {id: 4, label: "Saldo en inversiones en otros bancos", price:""}];

  onChange: (value: any) => void;

  onTouched: any = () => { }

  /**
   * Set the function to be called 
   * when the control receives a change event.
   */
  registerOnChange(onChange: (value: any) => void) {
    this.onChange = onChange;
  }

  /**
   * Set the function to be called 
   * when the control receives a touch event.
   */
  registerOnTouched(onTouched: any): void {
    this.onTouched = onTouched;
  }

  /**
   * Write a new value to the element.
   */
  writeValue(value: any): void {
    this.checks = value;
  }
  
}