import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TableCheckDataComponent } from './table-check-data.component';

describe('TableCheckDataComponent', () => {
  let component: TableCheckDataComponent;
  let fixture: ComponentFixture<TableCheckDataComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TableCheckDataComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TableCheckDataComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
