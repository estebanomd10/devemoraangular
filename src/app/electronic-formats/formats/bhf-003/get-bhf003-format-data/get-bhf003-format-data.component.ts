import { Component, OnInit, ViewChild, ElementRef, Output, EventEmitter, Input } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
//import { CatalogDataService } from '../../../../services/catalogsDataService';
import { HttpErrorResponse } from '@angular/common/http';
//import { GlobalFunctionsService } from '../../../services/global-functions.service';

import { matchingNums } from '../../../validators/custom-validators';

@Component({
  selector: 'app-get-bhf003-format-data',
  templateUrl: './get-bhf003-format-data.component.html',
  styleUrls: ['./get-bhf003-format-data.component.scss']
})
export class GetBhf003FormatDataComponent implements OnInit {
  @Input() customerData: any;
  customerDatos = {};

  displayComponent_1 = true;
  displayComponent_2 = false;
  displayComponent_3 = false;
  displayComponent_4 = false;
  displayComponent_5 = false;
  displayComponent_6 = false;

  constructor() { }

  ngOnInit() {
    this.customerDatos = this.customerData;
  }

  goToJob(event){
    this.customerDatos = event;
    this.displayComponent_1 = false;
    this.displayComponent_2 = true;
    this.displayComponent_3 = false;
    this.displayComponent_4 = false;
    this.displayComponent_5 = false;
    this.displayComponent_6 = false;
  }

  goToEconomicInformation(event){
    this.customerDatos = event;
    this.displayComponent_1 = false;
    this.displayComponent_2 = false;
    this.displayComponent_3 = true;
    this.displayComponent_4 = false;
    this.displayComponent_5 = false;
    this.displayComponent_6 = false;
  }

  goToHeritage(event){
    this.customerDatos = event;
    this.displayComponent_1 = false;
    this.displayComponent_2 = false;
    this.displayComponent_3 = false;
    this.displayComponent_4 = true;
    this.displayComponent_5 = false;
    this.displayComponent_6 = false;
  }
  
  goToPersonalReferences(event){
    this.customerDatos = event;
    this.displayComponent_1 = false;
    this.displayComponent_2 = false;
    this.displayComponent_3 = false;
    this.displayComponent_4 = false;
    this.displayComponent_5 = true;
    this.displayComponent_6 = false;
  }

  goToIndividualConsent(event){
    console.info("goToIndividualConsent " + JSON.stringify(event));
    this.customerDatos = event;
    this.displayComponent_1 = false;
    this.displayComponent_2 = false;
    this.displayComponent_3 = false;
    this.displayComponent_4 = false;
    this.displayComponent_5 = false;
    this.displayComponent_6 = true;
  }

  goToOtherComponents(event){
    this.customerDatos = event;
    this.displayComponent_1 = false;
    this.displayComponent_2 = false;
    this.displayComponent_3 = false;
    this.displayComponent_4 = false;
    this.displayComponent_5 = false;
    this.displayComponent_6 = false;
  }

}
