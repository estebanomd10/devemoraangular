import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GetBhf003RefereciasPersonalesComponent } from './get-bhf003-referecias-personales.component';

describe('GetBhf003RefereciasPersonalesComponent', () => {
  let component: GetBhf003RefereciasPersonalesComponent;
  let fixture: ComponentFixture<GetBhf003RefereciasPersonalesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GetBhf003RefereciasPersonalesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GetBhf003RefereciasPersonalesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
