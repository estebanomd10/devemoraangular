import { Component, OnInit, ViewChild, ElementRef, Output, EventEmitter, Input } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
//import { CatalogDataService } from '../../../../services/catalogsDataService';
import { HttpErrorResponse } from '@angular/common/http';
//import { GlobalFunctionsService } from '../../../services/global-functions.service';
import { matchingNums } from '../../../../validators/custom-validators';


@Component({
  selector: 'app-get-bhf003-referecias-personales',
  templateUrl: './get-bhf003-referecias-personales.component.html',
  styleUrls: ['./get-bhf003-referecias-personales.component.css', './get-bhf003-referecias-personales.component.scss']
})
export class GetBhf003RefereciasPersonalesComponent implements OnInit {
  @Output() showPersonalReferences: EventEmitter<any> = new EventEmitter<any>();
  @Input() customerData: any;
  customerDatos = {};

  referencias_personales = [];
   
  config_referencias_personales = {
    title: 'Referencias Personales (estos campos son obligatorios:)',
    fields: [
      {
        name: 'apellido_paterno',
        type: 'input',
        label: 'Apellido paterno',
        required: true,
      },
      {
        name: 'apellido_materno',
        type: 'input',
        label: 'Apellido materno',
        required: true,
      },
      {
        name: 'nombre',
        type: 'input',
        label: 'Nombre(s)',
        required: true,
      },
      {
        name: 'telefono',
        type: 'input',
        label: 'Teléfono(s)',
        required: true,
      }    
    ]
  };

  form_1: FormGroup;
//private catalogDataService: CatalogDataService  private globalFunctions: GlobalFunctionsService
  constructor(private fb: FormBuilder) { }

  ngOnInit() {
    this.customerDatos = this.customerData;

    this.form_1 = this.fb.group({
      'referencias_personales': [[], Validators.compose([Validators.required])]
    });
  }

  submit_form_1() {
    console.info("submit_form_6 " + JSON.stringify(this.customerDatos));
    this.showPersonalReferences.emit(this.customerDatos);
  }

  cancelFormat() {
    let response = confirm("¿Estás seguro que deseas cancelar este formato?");
    if (response == true) {
      //this.globalFunctions.gotoHome();
    }
  }

  
  gotoHome() {
    //this.globalFunctions.gotoHome();
  }
}
