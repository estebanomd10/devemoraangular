import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GetBhf003FormatDataComponent } from './get-bhf003-format-data.component';

describe('GetBhf003FormatDataComponent', () => {
  let component: GetBhf003FormatDataComponent;
  let fixture: ComponentFixture<GetBhf003FormatDataComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GetBhf003FormatDataComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GetBhf003FormatDataComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
