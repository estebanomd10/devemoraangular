import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GetBhf003EmpleoOcupacionComponent } from './get-bhf003-empleo-ocupacion.component';

describe('GetBhf003EmpleoOcupacionComponent', () => {
  let component: GetBhf003EmpleoOcupacionComponent;
  let fixture: ComponentFixture<GetBhf003EmpleoOcupacionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GetBhf003EmpleoOcupacionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GetBhf003EmpleoOcupacionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
