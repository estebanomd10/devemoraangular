import { Component, OnInit, ViewChild, ElementRef, Output, EventEmitter, Input } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
//import { CatalogDataService } from '../../../../services/catalogsDataService';
import { HttpErrorResponse } from '@angular/common/http';
//import { GlobalFunctionsService } from '../../../services/global-functions.service';

import { matchingNums } from '../../../../validators/custom-validators';

import { SubstepsComponent } from '../../../../components/substeps/substeps.component';
import { SelectComponent } from '../../../../components/select/select.component';

@Component({
  selector: 'app-get-bhf003-empleo-ocupacion',
  templateUrl: './get-bhf003-empleo-ocupacion.component.html',
  styleUrls: ['./get-bhf003-empleo-ocupacion.component.css','./get-bhf003-empleo-ocupacion.component.scss']
})
export class GetBhf003EmpleoOcupacionComponent implements OnInit {
  @Output() showJob: EventEmitter<any> = new EventEmitter<any>();
  @Input() customerData: any;
  customerDatos = {};
  @ViewChild(SubstepsComponent) substeps:SubstepsComponent;

  confirmation_options = [ {id: 1, label: "Sí"},{id: 2, label: "No"}];

  form_1: FormGroup;
  displayForm_1=false;
  colonia_options =[ {id: 1, label: "Lomas de guadalupe"}, {id: 2, label: "Lomas del carril"}];
  entidad_options= [ {id: 1, label: "Morelos"}, {id: 2, label: "Aguascalientes"}];
  pais_options=[ {id: 1, label: "México"}, {id: 2, label: "Estados Unidos"}];

  form_2: FormGroup;
  displayForm_2=false;
  displayIpunt_1=false;
  tipo_ingreso_options =[ {id: 1, label: "Asalariado"}, {id: 2, label: "Asalariado"}];
  tipo_contrato_options =[ {id: 1, label: "Contrato-1"}, {id: 2, label: "Otro"}];

  form_3: FormGroup;
  displayForm_3=false;

  form_4: FormGroup;
  displayIpunt_2=false;
  displayForm_4=false;
  sector_laboral_options=[ {id: 1, label: "Privado"}, {id: 2, label: "Publico"}];

  constructor(private fb: FormBuilder) { }

  ngOnInit() {
    this.customerDatos = this.customerData;

    this.form_1 = this.fb.group({
      'primerEmpleo': [{},Validators.required],
      'nombreEmpresa': [null, [Validators.required, Validators.pattern('^[a-zA-Z0-9 -\/ \sáéíóúñÁÉÍÓÚÑ]+$'), Validators.maxLength(50)]],
      'calle': [null, [Validators.required, Validators.pattern('^[a-zA-Z0-9 ]+$'), Validators.maxLength(22)]],
      'numeroExterior': [null, [Validators.required, Validators.pattern('^[0-9]*$'), Validators.maxLength(4)]], 
      'numeroInterior': [null, [Validators.required, Validators.pattern('^[0-9]*$'), Validators.maxLength(4)]],
      'cp': [null, [Validators.required, Validators.pattern('^[0-9]*$'), Validators.maxLength(4)]],
      'colonia':  [null, Validators.required],  
      'estado': [null, Validators.required], 
      'delegacion': [null, [Validators.required, Validators.pattern('^[a-zA-Z0-9 ]+$'), Validators.maxLength(22)]], 
      'paiss': [null, Validators.required]   
    });

    this.form_2 = this.fb.group({
      'tipoIngreso': [{},Validators.required],
      'tipoContrato': [null],
      'descripcionContrato': [null],
      'sueldo': [null], 
      'bono': [null],
      'comision': [null]    
    }); 

    this.form_3 = this.fb.group({
      'giro': [null],
      'departamento': [null],
      'puesto': [null], 
      'fecha': [null],
      'antiguedadAno': [null],
      'antiguedadMes': [null],
      'telefonoOficina': [null]
    }); 

    this.form_4 = this.fb.group({
      'sectorLaboral': [null, Validators.required],
      'funcionesActividades': [null],
      'nombreEmpresa': [null], 
      'causaSeparacion': [null],
      'fechaIngreso': [null],
      'fechaBaja': [null],
      'antiguedadAno': [null],
      'antiguedadMes': [null]
    });
  }

  submit_form_1(){
    this.substeps.showStep(2);
  }

  submit_form_2(){
    this.displayForm_3=true;
    this.form_3.controls['giro'].setValidators([Validators.required,Validators.pattern('^[a-zA-Z0-9 -\/ \sáéíóúñÁÉÍÓÚÑ]+$'), Validators.maxLength(50)]);
    this.form_3.controls['departamento'].setValidators([Validators.required,Validators.pattern('^[a-zA-Z0-9 -\/ \sáéíóúñÁÉÍÓÚÑ]+$'), Validators.maxLength(50)]);
    this.form_3.controls['puesto'].setValidators([Validators.required,Validators.pattern('^[a-zA-Z0-9 -\/ \sáéíóúñÁÉÍÓÚÑ]+$'), Validators.maxLength(50)]);
    this.form_3.controls['fecha'].setValidators(Validators.required);
    this.form_3.controls['antiguedadAno'].setValidators([Validators.required,Validators.pattern('^[a-zA-Z0-9 -\/ \sáéíóúñÁÉÍÓÚÑ]+$'), Validators.maxLength(10)]);
    this.form_3.controls['antiguedadMes'].setValidators([Validators.pattern('^[a-zA-Z0-9 -\/ \sáéíóúñÁÉÍÓÚÑ]+$'), Validators.maxLength(10)]);
    this.form_3.controls['telefonoOficina'].setValidators([Validators.required,Validators.pattern('^[0-9]*$'), Validators.maxLength(10)]);
  }

  submit_form_3(){
    this.substeps.showStep(3);
  }

  submit_form_4(){
    console.info("submit_form_4 " + JSON.stringify(this.customerDatos));
    this.showJob.emit(this.customerDatos);
  }

  selectionChangedForPrimerEmpledo(){
    this.displayForm_1 =true;
  }

  selectionChangedForTipoIngreso(){
    this.displayForm_2 =true;
    this.form_2.controls['tipoContrato'].setValidators(Validators.required);
    this.form_2.controls['sueldo'].setValidators([Validators.required,Validators.pattern('^[0-9]*$'), Validators.maxLength(50)]);
    this.form_2.controls['bono'].setValidators([Validators.required,Validators.pattern('^[0-9]*$'), Validators.maxLength(50)]);
    this.form_2.controls['comision'].setValidators([Validators.required,Validators.pattern('^[0-9]*$'), Validators.maxLength(50)]);
  }

  selectionChangedForTipoContacto(){
    this.displayIpunt_1 =true;
    this.form_2.controls['descripcionContrato'].setValidators([Validators.required,Validators.pattern('^[a-zA-Z0-9 -\/ \sáéíóúñÁÉÍÓÚÑ]+$'), Validators.maxLength(50)]);
  }

  selectionChangedForSectorLaboral(){
    this.displayIpunt_2 =true;
    this.form_4.controls['funcionesActividades'].setValidators([Validators.required,Validators.pattern('^[a-zA-Z0-9 -\/ \sáéíóúñÁÉÍÓÚÑ]+$'), Validators.maxLength(50)]);
    this.form_4.controls['nombreEmpresa'].setValidators([Validators.required,Validators.pattern('^[a-zA-Z0-9 -\/ \sáéíóúñÁÉÍÓÚÑ]+$'), Validators.maxLength(50)]);
    this.form_4.controls['causaSeparacion'].setValidators([Validators.required,Validators.pattern('^[a-zA-Z0-9 -\/ \sáéíóúñÁÉÍÓÚÑ]+$'), Validators.maxLength(50)]);
    this.form_4.controls['fechaIngreso'].setValidators(Validators.required);
    this.form_4.controls['fechaBaja'].setValidators(Validators.required);
    this.form_4.controls['antiguedadAno'].setValidators([Validators.required,Validators.pattern('^[a-zA-Z0-9 -\/ \sáéíóúñÁÉÍÓÚÑ]+$'), Validators.maxLength(10)]);
    this.form_4.controls['antiguedadMes'].setValidators([Validators.pattern('^[a-zA-Z0-9 -\/ \sáéíóúñÁÉÍÓÚÑ]+$'), Validators.maxLength(10)]);
  }
}
