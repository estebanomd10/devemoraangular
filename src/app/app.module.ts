//import { App } from './application';
import { BrowserModule } from '@angular/platform-browser';
//import { NgxCurrencyModule } from "ngx-currency";
import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
//import { HttpModule } from '@angular/http';
import { RouterModule } from '@angular/router';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';


import { AppComponent } from './app.component';
import { AppRoutingModule } from './app.routing';

import { Bhf003Component } from './electronic-formats/formats/bhf-003/bhf-003.component';
import { GetBhf003FormatDataComponent } from './electronic-formats/formats/bhf-003/get-Bhf003-format-data/get-Bhf003-format-data.component';
import { GetBhf003DatosPersonalesSolicitanteComponent } from './electronic-formats/formats/bhf-003/get-bhf003-format-data/get-bhf003-datos-personales-solicitante/get-bhf003-datos-personales-solicitante.component';
import { GetBhf003EmpleoOcupacionComponent } from './electronic-formats/formats/bhf-003/get-bhf003-format-data/get-bhf003-empleo-ocupacion/get-bhf003-empleo-ocupacion.component';
import { GetBhf003InformacionEconomicaComponent } from './electronic-formats/formats/bhf-003/get-bhf003-format-data/get-bhf003-informacion-economica/get-bhf003-informacion-economica.component';
import { GetBhf003PatrimonioComponent } from './electronic-formats/formats/bhf-003/get-bhf003-format-data/get-bhf003-patrimonio/get-bhf003-patrimonio.component';
import { GetBhf003RefereciasPersonalesComponent } from './electronic-formats/formats/bhf-003/get-bhf003-format-data/get-bhf003-referecias-personales/get-bhf003-referecias-personales.component';
import { GetBhf003ConsentimientoIndividualComponent } from './electronic-formats/formats/bhf-003/get-bhf003-format-data/get-bhf003-consentimiento-individual/get-bhf003-consentimiento-individual.component';

import { FormatTitleComponent } from './electronic-formats/components/format-title/format-title.component';
import { RadioGroupComponent } from './electronic-formats/components/radio-group/radio-group.component';
import { SelectComponent } from './electronic-formats/components/select/select.component';
import { StepsComponent } from './electronic-formats/components/steps/steps.component';
import { ModalComponent } from './electronic-formats/components/modal/modal.component';
import { GetCustomerDataComponent } from './electronic-formats/components/get-customer-data/get-customer-data.component';
import { EfSidebarComponent } from './electronic-formats/components/ef-sidebar/ef-sidebar.component';
import { EditableGridComponent } from './electronic-formats/components/editable-grid/editable-grid.component';
import { SubstepsComponent } from './electronic-formats/components/substeps/substeps.component';
import { DataGridComponent } from './electronic-formats/components/data-grid/data-grid.component';

@NgModule({
 declarations: [
   AppComponent,
   Bhf003Component,
   FormatTitleComponent,
   RadioGroupComponent,
   SelectComponent,
   StepsComponent,
   ModalComponent,
   GetCustomerDataComponent,
   EfSidebarComponent,
   EditableGridComponent,
   SubstepsComponent,
   DataGridComponent,
   GetBhf003FormatDataComponent,//news
   GetBhf003DatosPersonalesSolicitanteComponent,
   GetBhf003EmpleoOcupacionComponent,
   GetBhf003InformacionEconomicaComponent,
   GetBhf003PatrimonioComponent,
   GetBhf003RefereciasPersonalesComponent,
   GetBhf003ConsentimientoIndividualComponent
 ],
 imports: [ BrowserModule, RouterModule, FormsModule , ReactiveFormsModule ,AppRoutingModule],
 schemas: [ CUSTOM_ELEMENTS_SCHEMA ],
 providers: [],
 bootstrap: [AppComponent]
})
export class AppModule { }